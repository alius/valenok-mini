'''
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
'''

import re
import sys
import string
import random
import gevent
import requests

import time

from gevent import monkey
from gevent.queue import Queue
from gevent.queue import Empty

monkey.patch_all(thread=False)

class ParseWorker(gevent.Greenlet):

    running = True
    linkre = re.compile('href=[\'"]?([^\'" >]+)')
    postre = re.compile('action=[\'"]?([^\'" >]+)')

    def __init__(self, parseQueue, manager, finder = None):
        gevent.Greenlet.__init__(self)
        self.manager = manager
        self.parseQueue = parseQueue
        self.finder = finder
        self.findForms = manager.findForms()
        self.tag = "".join(random.choice(string.letters) for i in xrange(15))

    def stop(self):
        self.running = False

    def setTag(self, tag):
        self.tag = tag

    def getTag(self):
        return self.tag

    def _run(self):
        while self.running:
            packet = self.parseQueue.get()
            if packet is not None and "data" in packet:
                self.manager.addBatch(packet["url"], self.extract(packet["data"]))
            else:
                self.stop()

    def extract(self, source):
        STARTS = ('mailto', 'javascript', '#', '&quot;', 'https://ssl', '://', '#')
        ENDS = ('.css', '.js', '.jpg', '.gif', '.png', '.flv', '.mpeg', '.mp4', '.ico', '.dtd', '.jpeg', '.zip', '.rar', '.gz', '.gzip', '.bzip2', '.bzip', '.pdf', '.wmv', '.JPG', '.xls', '.doc', '.ppt')

        source = source.replace("&quot;", '"') 
        L = self.linkre.findall(source)
        links = set([i for i in L if not i.endswith(ENDS) and not i.startswith(STARTS)])

        post = set(self.postre.findall(source)) if self.findForms else None
        user = self.finder(source) if self.finder else None

        return {"links": links, "user": user, "post": post}

class FetchWorker(gevent.Greenlet):

    tag = None
    running = True

    def __init__(self, fetchQueue, parseQueue, manager, auth=None):
        gevent.Greenlet.__init__(self)
        #self.s = requests.session()
        self.manager = manager
        self.fetchQueue = fetchQueue
        self.parseQueue = parseQueue
        if auth is not None:
            self.auth = tuple(auth.split(":"))
        else:
            self.auth = None

        self.tag = "".join(random.choice(string.letters) for i in xrange(10))

    def stop(self):
        self.running = False

    def setTag(self, tag):
        self.tag = tag

    def getTag(self):
        return self.tag

    def _run(self):
        while self.running:
            try:
                url = self.fetchQueue.get(True, 3)
            except Empty, e:
                self.parseQueue.put(None)
                self.stop()
                continue

            if url is not None:
                if not url.startswith("http://") and not url.startswith("https://"):
                    url = "".join(["http://", url])
                try:
                    if self.auth is not None:
                        response = requests.get(url, auth=self.auth)
                    else:
                        response = requests.get(url)
                    if response.text:
                        data = response.text
                        data = data.replace('&amp;', '&').replace("&quot;", '"')
                        self.manager.visited(url)
                        self.parseQueue.put({"url": url, "data": data})
                except Exception, e:
                    pass
            else:
                self.parseQueue.put("")
                self.stop()
