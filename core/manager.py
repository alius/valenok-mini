'''
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
'''

import re
import sys
import time

from core.reporter import Reporter

class Manager():

    ttl = None
    limit = 99999999
    reporter = None
    forceStop = False
    findFormUrl = False
    storeExternal = False
    visitedLinks = {}
    externalLinks = {}
    queuedLinks = {}
    postLinks = {}
    userData = []

    def __init__(self, fetchQueue):
        self.start = time.time()
        self.fetchQueue = fetchQueue
        self.reporter = Reporter.factory('stdout')

    ''' Sets limit for url '''
    def setLimit(self, limit):
        if limit is not None:
            self.limit = int(limit)

    ''' Sets the crawler ttl '''
    def setTtl(self, ttl):
        self.ttl = float(ttl)

    ''' Tells the crawler to keep links to external websites '''
    def setStoreExternal(self, store):
        self.storeExternal = store

    ''' Tells the crawler whether it should search for urls in forms '''
    def setFindForms(self, find):
        self.findFormUrl = find

    ''' Sets the current crawlable domain '''
    def setDomain(self, domain):
        self.currentDomain = domain
        if domain.startswith("http://"):
            self.urlPrefix = "http://"
            self.shortDomain = domain[7:]
        if self.shortDomain.startswith('www.'):
            self.urlPrefix += "www."
            self.shortDomain = self.shortDomain[4:]
        self.shortDomain = self.shortDomain.split('/')[0]

    ''' Should the parser look for urls in forms '''
    def findForms(self):
        return self.findFormUrl

    def close(self):
        if self.reporter is not None:
            self.reporter.out(self.visitedLinks.keys()[0:self.limit])
            if self.storeExternal:
                print "\nExternal links found"
                self.reporter.out(self.externalLinks.keys())
            if self.findFormUrl and len(self.postLinks) > 0:
                print "\nForm links found"
                self.reporter.out(self.postLinks.keys())
            if self.userData and len(self.userData) > 0:
                print ""
                print "Other information found"
                self.reporter.out(self.userData)

    def __fixUrl(self, parent, url):
        url = url.replace('%3A', ':').replace('%2F', '/').replace('%3F', '?').replace('%3D', '=').replace('&amp;', '&').strip()
        if url.startswith("http://"):
            domain = url[7:]
            if domain.startswith("www"): domain = domain[4:]
            if not domain.startswith(self.shortDomain):
                if self.storeExternal: self.externalLinks[domain] = True
                return
        elif url.startswith("https://"):
            return
        elif url.startswith("callto://"):
            return
        elif url.startswith("tel:"):
            return
        elif url.startswith("www."):
            domain = url[4:]
            if not domain.startswith(self.shortDomain):
                if self.storeExternal: self.externalLinks[domain] = True
                return
        elif url.startswith("/"):
            url = self.urlPrefix + self.shortDomain + url
        elif url.startswith("?"):
            if parent.endswith("/"):
                url = parent + url
            else:
                url = parent + "/" + url
        elif url.startswith(".."):
            level = len(url.split('..')) - 1
            if parent.endswith("/"): parent = parent[0:-1]
            url = "/" + url.replace("../", "")
            url = "/".join(parent.split("/")[0:-level]) + url
        elif url.startswith("./"):
            if parent.endswith("/"):
                url = parent + url[2:]
            else:
                url = parent + "/" + url[2:]
        else:
            if parent.endswith("/"): url = parent + url
            else: pass # FIXME" test more this case

        return url

    ''' Adds single url to the list '''
    def add(self, parent, url, post=False):
        url = self.__fixUrl(parent, url)
        if url not in self.visitedLinks and url not in self.queuedLinks:
            if self.ttl is not None:
                diff = time.time() - self.start
                if diff > self.ttl:
                    self.fetchQueue.queue.clear()
                    self.fetchQueue.put(None)
                    self.forceStop = True
                    return
            if len(self.queuedLinks) >= self.limit:
                if not self.forceStop:
                    self.fetchQueue.queue.clear()
                    self.fetchQueue.put(None)
                    self.forceStop = True
                    return

            if not post:
                self.queuedLinks[url] = True
                self.fetchQueue.put(url)
            else:
                self.postLinks[url] = True

    ''' Adds lists of urls to the list '''
    def addBatch(self, parent, urls):
        for url in urls["links"]: self.add(parent, url)
        if urls["post"] is not None:
            for url in urls["post"]: self.add(parent, url, True)
        if urls["user"]: self.userData += urls["user"]

    ''' Returns short domain name without protocol and www prefix '''
    def __normalize(self, uri):
        a = uri
        if not uri: return
        if uri[:8] == 'https://':
            uri = uri[8:]
        elif uri[:7] == 'http://':
            uri = uri[7:]
        uri = uri.split('/')[0].split('?')[0].split('%')[0].split(':')[0].split('\\')[0]
        return uri

    ''' Returns a list of visited links '''
    def getVisited(self):
        for k in self.visitedLinks.keys():
            print " > %s" % k
        return self.visitedLinks.keys()

    def visited(self, url):
        self.visitedLinks[url] = True
