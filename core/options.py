'''
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
'''

import sys
import ConfigParser

from optparse import OptionError
from optparse import OptionGroup
from optparse import OptionParser
from optparse import SUPPRESS_HELP

from core.data import conf

COLOR_RED = "\033[91m"
COLOR_GREEN = "\033[92m"
COLOR_BLUE = "\033[94m"
COLOR_CYAN = "\033[96m"
COLOR_WHITE = "\033[97m"
COLOR_YELLOW = "\033[93m"
COLOR_MAGENTA = "\033[95m"
COLOR_GREY = "\033[90m"
COLOR_ENDC = "\033[0m"

usage = COLOR_RED + "Valenok -- yet another website crawler\n\n" + COLOR_ENDC + \
          COLOR_YELLOW + "Main configuration\n" + COLOR_ENDC + \
          COLOR_GREY + "--url\t\turl to crawl without http prefix\n" + COLOR_ENDC + \
          COLOR_GREY + "--find\t\tFinds specific text on a webpage\n" + COLOR_ENDC + \
          COLOR_YELLOW + "\nThreads\n" + COLOR_ENDC + \
          COLOR_GREY + "--threads\tNumber of threads to spawn. Defaults to 2\n" + COLOR_ENDC + \
          COLOR_YELLOW + "\nMiscellaneous options\n" + COLOR_ENDC + \
          COLOR_GREY + "--auth\t\tBasic auth in the following format USERNAME:PASSWORD\n" + COLOR_ENDC + \
          COLOR_GREY + "--find-forms\tFind urls in forms\n" + COLOR_ENDC + \
          COLOR_GREY + "--ttl\t\tNumber of minutes for the crawler to run. Can be set using float values. Default is 1h\n" + \
          "--limit\t\tNumber of urls to crawl\n" + COLOR_ENDC + \
          COLOR_GREY + "--store-external Keep links to external websites\n" + COLOR_ENDC + \
          COLOR_YELLOW + "\nExample usage\n" + COLOR_ENDC + \
          COLOR_GREY + "python spider.py --url www.google.com --ttl 1\n" + COLOR_ENDC

def help():
    print usage
    exit()

def cmdArgParser():
    '''
    Parse command line arguments
    '''
    parser = OptionParser(usage=usage)

    try:
        params = OptionGroup(parser, "Params", "Spider params")
        params.add_option("--url", type="string", dest="url", help="Target url")
        params.add_option("--find", type="string", dest="find", help="Finds specific text on a webpage")
        params.add_option("--threads", default=2, type="int", dest="threads", help="Number of threads to spawn")
        params.add_option("--auth", type="string", dest="auth", help="Basic auth credentials")
        params.add_option("--ttl", default=60.0, type="float", dest="ttl", help="Number of minutes for the crawler to work")
        params.add_option("--limit", default=1000, type="int", dest="limit", help="Number of urls to crawl")
        params.add_option("--store-external", action="store_true", dest="store_external", help="Store external website links")
        params.add_option("--find-forms", action="store_true", dest="find_forms", help="Find urls in forms")

        parser.add_option_group(params)

        args = []

        for arg in sys.argv:
            args.append(arg)

        (args, _) = parser.parse_args(args)

        if not args.url: help()

        if args.url[:7] != "http://" and args.url[:8] != "https://":
            args.url = "".join(["http://", args.url])
        if not args.url.endswith("/"):
            args.url += "/"

        return args

    except (OptionError, TypeError), e:
        parser.error(e)

    except SystemExit, _:
        raise
