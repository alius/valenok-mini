'''
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
'''

import sys
import gevent

from core import finders
from core.data import conf
from core.data import manager
from core.data import fetchQueue
from core.data import parseQueue
from core.options import cmdArgParser
from core.workers import FetchWorker
from core.workers import ParseWorker

def prepare():
    arguments = cmdArgParser()
    conf.init(arguments.__dict__)
    manager.setDomain(conf.url)
    manager.setLimit(conf.limit)
    manager.setTtl(conf.ttl)
    manager.setFindForms(conf.find_forms)
    manager.setStoreExternal(conf.store_external)

def start():
    try:
        f = getattr(finders, conf.find)
    except (AttributeError, TypeError):
        f = None

    threads = list()
    threads.append(ParseWorker(parseQueue, manager, f))
    threads[0].start()

    for i in xrange(0, conf.threads):
        threads.append(FetchWorker(fetchQueue, parseQueue, manager, conf.auth))
        threads[i+1].start()

    fetchQueue.put_nowait(conf.url)
    gevent.joinall(threads)

def stop():
    #manager.getVisited()
    manager.close()
    sys.exit()
