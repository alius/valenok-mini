'''
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
'''

class Reporter:

    @staticmethod
    def factory(driver):
        if driver == "stdout":
            return StdoutReport()
        else:
            raise ValueError

class Driver:

    def out(self, data):
        raise NotImplementedError


class StdoutReport(Driver):

    # colors taken from
    # http://stackoverflow.com/questions/287871/print-in-terminal-with-colors-using-python
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

    def out(self, data):
        for k in data:
            print " > %s" % k
